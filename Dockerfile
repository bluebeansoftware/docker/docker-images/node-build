FROM node:lts-alpine

RUN set -x \
    && npm install -g @vue/cli@latest firebase-tools @angular/cli \
    && npm cache clean --force \
    && rm -rf \
    /tmp/* \
    /root/.npm/* \
    /root/.cache/* \
    /var/cache/apk/*
